import {Component, OnInit} from '@angular/core';
import {Team} from '../../data/team.model';
import {TeamsService} from '../teams.service';
import {YesNoDialogService} from '../../yes-no-dialog/yes-no-dialog.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.css']
})
export class ListTeamsComponent implements OnInit {

  teams: Array<Team>;

  constructor(private teamsService: TeamsService,
              private matSnackbar: MatSnackBar,
              private yesNoDialogService: YesNoDialogService) {

  }

  ngOnInit(): void {
    this.teamsService.listTeams()
      .subscribe(value => {
        this.teams = value;
      });
  }

  deleteTeam(team: Team) {
    this.yesNoDialogService.openMatDialog(
      'Delete Team ' + team.name, 'Are You sure you want to delete team with id' + team.id,
      () => {
        this.teamsService
          .deleteTeam(team.id)
          .subscribe(deleted => {
            if (deleted) {
              this.matSnackbar.open('Team Deleted ..', 'ok');
              window.location.reload();
            }
          });
      }, () => {
      }
    );
  }
}
