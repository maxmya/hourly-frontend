import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TeamsService} from '../teams.service';
import {Team} from '../../data/team.model';
import {MatTableDataSource} from '@angular/material/table';
import {User} from '../../data/user.model';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {


  displayedColumns: string[] = ['fullName', 'email', 'role'];
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  teamForDetails: Team;

  constructor(private activatedRoute: ActivatedRoute,
              private teamsService: TeamsService) {
  }

  ngOnInit(): void {
    this.activatedRoute
      .params
      .subscribe(value => {
        let teamId = value.id;
        this.teamsService.getTeamDetails(teamId)
          .subscribe(team => {
            this.teamForDetails = team;
            this.dataSource.data = this.teamForDetails.members;
            this.dataSource.paginator = this.paginator;
          });
      });

  }

}
