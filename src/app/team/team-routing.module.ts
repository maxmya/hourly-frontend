import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListTeamsComponent} from './list-teams/list-teams.component';
import {CreateTeamComponent} from './create-team/create-team.component';
import {AdminGuard} from '../admin.guard';
import {TeamDetailsComponent} from './team-details/team-details.component';

const routes: Routes = [
  {path: '', component: ListTeamsComponent, canActivate: [AdminGuard]},
  {path: 'add', component: CreateTeamComponent, canActivate: [AdminGuard]},
  {path: 'details/:id', component: TeamDetailsComponent, canActivate: [AdminGuard]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule {
}
