import {Injectable} from '@angular/core';
import {Team} from '../data/team.model';
import {Observable} from 'rxjs';
import {User} from '../data/user.model';
import {AngularFirestore} from '@angular/fire/firestore';
import {TEAMS, USERS} from '../data/keys.model';
import {AngularFireStorage} from '@angular/fire/storage';
import {finalize, tap} from 'rxjs/operators';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {


  constructor(private firestore: AngularFirestore,
              private firebaseAuth: AngularFireAuth,
              private storage: AngularFireStorage) {
  }

  getTeamDetails(teamId: string): Observable<Team> {
    return new Observable<Team>(subscriber => {
      this.firestore
        .collection(TEAMS)
        .doc(teamId)
        .get()
        .subscribe(snap => {
          let team = snap.data() as Team;
          subscriber.next(team);
          subscriber.complete();
        });
    });
  }

  listAdmins(): Observable<Array<User>> {
    return new Observable<Array<User>>(subscriber => {
      let admins = new Array<User>();
      this.firestore
        .collection(USERS)
        .get()
        .subscribe(value => {
          value.docs.forEach(userSnap => {
            let user = userSnap.data() as User;
            if (user.role.id == 0) {
              admins.push(user);
            }
          });
          subscriber.next(admins);
          subscriber.complete();
        });
    });
  }

  addTeam(team: Team, coverFile: File): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      const path = `teams/${Date.now()}_${coverFile.name}`;
      const ref = this.storage.ref(path);
      let task = this.storage.upload(path, coverFile);
      task.snapshotChanges().pipe(
        tap(console.log),
        finalize(async () => {
          team.coverUrl = await ref.getDownloadURL().toPromise();
          team.id = this.firestore.createId();

          this.firebaseAuth.user
            .subscribe(currentUser => {
              this.firestore
                .collection(USERS)
                .doc(currentUser.uid)
                .get()
                .subscribe(userSnap => {
                  let user = userSnap.data() as User;
                  let members = new Array<User>();
                  members.push(user);
                  team.members = members;
                  this.firestore.collection(TEAMS).doc(team.id).set(team);
                  subscriber.next(true);
                  subscriber.complete();
                });
            });

        })).subscribe();
    });
  }

  deleteTeam(teamId: string): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.firestore
        .collection(TEAMS)
        .doc(teamId)
        .delete().then(value => {
        subscriber.next(true);
        subscriber.complete();
      }).catch(reason => {
        subscriber.next(false);
        subscriber.complete();
      });
    });
  }

  listTeams(): Observable<Array<Team>> {
    return new Observable<Array<Team>>(subscriber => {
      let teams = new Array<Team>();
      this.firestore
        .collection(TEAMS)
        .get()
        .subscribe(value => {
          value.docs.forEach(teamSnap => {
            let team = teamSnap.data() as Team;
            teams.push(team);
          });
          subscriber.next(teams);
          subscriber.complete();
        });
    });
  }


}
