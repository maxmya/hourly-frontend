import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {TeamsService} from '../teams.service';
import {isValidTeam, Team} from '../../data/team.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {User} from '../../data/user.model';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {

  coverImageAsBase64String: any;
  coverImageAsFile: File;
  loading = false;
  admins: Array<User>;

  constructor(private teamsService: TeamsService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.loadAdmins();
  }


  loadAdmins() {
    this.teamsService.listAdmins().subscribe(value => {
      this.admins = value;
    });
  }

  submitTeam(addTeamForm: NgForm) {
    let team = addTeamForm.value as Team;
    this.loading = true;
    if (!isValidTeam(team, this.coverImageAsFile)) {
      this.openSnackBar('Please Fill Missing Data');
      this.loading = false;
      return;
    }
    this.teamsService
      .addTeam(team, this.coverImageAsFile)
      .subscribe(value => {
        if (value) {
          addTeamForm.resetForm();
          this.openSnackBar('Team Added Successfully');
          this.coverImageAsFile = null;
          this.coverImageAsBase64String = null;
        } else {
          this.openSnackBar('Cannot Add Team Now , Please Try Again Later.');
        }
        this.loading = false;
      });
  }

  openSnackBar(message: string) {
    this.matSnackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

  handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.coverImageAsBase64String = btoa(binaryString);
  }

  onUploadChange(evt: any) {
    const files = evt.target.files;
    const file = files[0];
    this.coverImageAsFile = file;
    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
}
