export class ResponseWrapper<T> {
  data: T;
  message: string;
  success: boolean;
}
