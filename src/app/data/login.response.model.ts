export class LoginResponse {
  token: string;
  username: string;
  fullName: string;
  profileUrl: string;
}
