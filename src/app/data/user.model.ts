import {Role} from './role.model';
import {Team} from './team.model';

export class User {
  id: string;
  email: string;
  password: string;
  fullName: string;
  profilePicture: string;
  team: Team;
  role: Role;
}
