import {User} from './user.model';

export class Team {
  id: string;
  name: string;
  description: string;
  leader: User;
  createdAt: string;
  coverUrl: String;
  members: Array<User>;


}

export function isValidTeam(team: Team, file: File): boolean {
  return (team.name != null && team.name != '' && file != null);
}
