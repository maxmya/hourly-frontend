import {Component, OnInit} from '@angular/core';
import {AppCookiesService} from '../../service/app-cookies.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  private readonly DASHBOARD = 'Dashboard';
  private readonly CREATE_TEAM = 'Create Team';
  private readonly LIST_TEAMS = 'Teams';
  private readonly TEAM_DETAILS = 'Team Details';
  private readonly LIST_USERS = 'Users';
  private readonly ADD_USER = 'Add User';
  private readonly LIST_TASKS = 'Tasks';
  private readonly ASSIGN_TASK = 'Assign Task';

  title = '';

  constructor(public appCookiesService: AppCookiesService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.changeTitle();
    this.router.events.subscribe(_ => {
      this.changeTitle();
    });
  }


  changeTitle() {
    switch (this.router.url) {
      case '/':
        this.title = this.DASHBOARD;
        break;
      case '/team/add':
        this.title = this.CREATE_TEAM;
        break;
      case '/team':
        this.title = this.LIST_TEAMS;
        break;
      case '/users':
        this.title = this.LIST_USERS;
        break;
      case '/users/add':
        this.title = this.ADD_USER;
        break;
      case '/tasks':
        this.title = this.LIST_TASKS;
        break;
      case '/tasks/assign':
        this.title = this.ASSIGN_TASK;
        break;
    }
    if (this.router.url.includes('/team/details/')) {
      this.title = this.TEAM_DETAILS;
    }
  }
}
