import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MainGuard} from '../main.guard';
import {AdminGuard} from '../admin.guard';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, canActivate: [MainGuard], children: [
      {
        path: 'team',
        loadChildren: () => import('../team/team.module').then(m => m.TeamModule),
        canActivate: [AdminGuard]
      },
      {
        path: 'users',
        loadChildren: () => import('../users/users.module').then(m => m.UsersModule),
        canActivate: [AdminGuard]
      },
      {
        path: 'tasks',
        loadChildren: () => import('../tasks/tasks.module').then(m => m.TasksModule),
        canActivate: [AdminGuard]
      },
      {
        path: 'projects',
        loadChildren: () => import('../projects/projects.module').then(m => m.ProjectsModule),
        canActivate: [AdminGuard]
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {


}
