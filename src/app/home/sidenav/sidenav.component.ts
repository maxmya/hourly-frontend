import {Component, OnInit} from '@angular/core';
import {AppCookiesService} from '../../service/app-cookies.service';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {log} from 'util';

@Component({
  selector: 'sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  constructor(public appCookiesService: AppCookiesService,
              public authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  logOut() {
    this.authService
      .logoutUser()
      .subscribe(loggedOut => {
        if (loggedOut) {
          this.router.navigate(['/auth']).then(_ => {
            this.appCookiesService.deleteAllData();
          });
        } else {
          console.log('ERROR');
        }
      });

  }
}
