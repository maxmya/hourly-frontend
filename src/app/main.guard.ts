import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class MainGuard implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.authService
        .validateUserAuthentication()
        .subscribe(loggedIn => {
          if (!loggedIn) {
            this.router.navigate(['auth']);
          }
          subscriber.next(loggedIn);
          subscriber.complete();
        }, error => {
          console.log(error);
          subscriber.next(false);
          subscriber.complete();
        });
    });
  }

}
