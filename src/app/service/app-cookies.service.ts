import {Injectable} from '@angular/core';
import {NgxEncryptCookieService} from 'ngx-encrypt-cookie';

@Injectable({providedIn: 'root'})
export class AppCookiesService {

  private readonly DISPLAY_KEY = 'display';
  private readonly PICTURE_URL = 'pic-url';

  // this will be obfuscated - 32 char key
  private readonly cookiesEncryptionKey = '65ea88e33f6769547e5cb19ad844227a';

  constructor(private encryptedCookies: NgxEncryptCookieService) {
  }

  saveDisplayName(displayName: string) {
    this.encryptedCookies.set(this.DISPLAY_KEY, displayName, true, this.cookiesEncryptionKey);
  }

  getDisplayName(): string {
    return this.encryptedCookies.get(this.DISPLAY_KEY, true, this.cookiesEncryptionKey);
  }

  savePictureUrl(displayName: string) {
    this.encryptedCookies.set(this.PICTURE_URL, displayName, true, this.cookiesEncryptionKey);
  }

  getPictureUrl() {
    return this.encryptedCookies.get(this.PICTURE_URL, true, this.cookiesEncryptionKey);
  }


  deleteAllData() {
    this.encryptedCookies.delete(this.DISPLAY_KEY);
    this.encryptedCookies.delete(this.PICTURE_URL);
  }

}
