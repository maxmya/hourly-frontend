import {Injectable} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {MessageDialog} from "./message-dialog";

@Injectable({
  providedIn: 'root'
})
export class MessageDialogService {

  constructor(private matDialog: MatDialog) {
  }


  openMatDialog(title: string, message: string) {
    this.matDialog.open(MessageDialog, {data: {title: title, message: message}})
  }

}
