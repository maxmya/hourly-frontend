import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {TeamsService} from '../../team/teams.service';
import {Team} from '../../data/team.model';
import {Role} from '../../data/role.model';
import {UsersService} from '../users.service';
import {User} from '../../data/user.model';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {


  profileImageAsBase64String: any;
  profileImageAsFile: File;
  availableTeams: Array<Team>;
  availableRoles: Array<Role>;
  loading = false;

  constructor(private teamsService: TeamsService,
              private snackBar: MatSnackBar,
              private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.teamsService
      .listTeams()
      .subscribe(value => {
        this.availableTeams = value;
      });

    this.usersService
      .listRoles()
      .subscribe(value => {
        this.availableRoles = value;
      });
  }

  handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.profileImageAsBase64String = btoa(binaryString);
  }

  onUploadChange(evt: any) {
    const files = evt.target.files;
    const file = files[0];
    this.profileImageAsFile = file;
    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  submitUser(addUserForm: NgForm) {
    this.loading = true;
    let user = addUserForm.value as User;

    if (user.email == '' || user.password == '' || user.fullName == ''
      || user.team == null || user.role == null) {
      this.snackBar.open('Please Fill Missing Data', 'OK');
      this.loading = false;
      return;
    }

    this.usersService
      .addUser(user, this.profileImageAsFile)
      .subscribe(done => {
        this.loading = false;
        if (done) {
          this.snackBar.open('User Created Successfully', 'OK');
          addUserForm.resetForm();
        } else {
          this.snackBar.open('User Is Not Created Please Try Again', 'OK');
        }
      }, error => {
        this.loading = false;
        this.snackBar.open(error, 'OK');
      });
  }
}
