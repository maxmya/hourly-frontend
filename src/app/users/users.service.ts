import {Injectable} from '@angular/core';
import {Role} from '../data/role.model';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {ROLES, TEAMS, USERS, UTILS} from '../data/keys.model';
import {User} from '../data/user.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {finalize, tap} from 'rxjs/operators';
import {AngularFireStorage} from '@angular/fire/storage';
import {Team} from '../data/team.model';

export class BlankProfile {
  url: string;
}


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private firebaseFirestore: AngularFirestore,
              private storage: AngularFireStorage,
              private firebaseAuth: AngularFireAuth) {
  }

  listUsers(): Observable<Array<User>> {
    return new Observable<Array<User>>(subscriber => {
      let users = new Array<User>();
      this.firebaseFirestore
        .collection(USERS)
        .get()
        .subscribe(usersSnaps => {
          usersSnaps.docs.forEach(usersSnap => {
            let user = usersSnap.data() as User;
            users.push(user);
          });
          subscriber.next(users);
          subscriber.complete();
        });
    });
  }

  listRoles(): Observable<Array<Role>> {
    return new Observable<Array<Role>>(subscriber => {
      let roles = new Array<Role>();
      this.firebaseFirestore
        .collection(ROLES)
        .get()
        .subscribe(value => {
          value.docs.forEach(roleSnap => {
            let role = roleSnap.data() as Role;
            roles.push(role);
          });
          subscriber.next(roles);
          subscriber.complete();
        });

    });
  }

  addUser(user: User, profilePicture: File): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.firebaseAuth
        .createUserWithEmailAndPassword(user.email, user.password)
        .then(async value => {
          if (value.user != null) {
            delete user.password;
            user.id = value.user.uid;
            await this.addUserToTeam(user);
            if (profilePicture == null) {
              this.saveUserWithoutImageFile(user);
            } else {
              this.saveUserWithImageFile(user, profilePicture);
            }
            subscriber.next(true);
            subscriber.complete();
          } else {
            subscriber.next(false);
            subscriber.complete();
          }
        }).catch(reason => {
        subscriber.error(reason);
        subscriber.complete();
      });
    });
  }

  async addUserToTeam(user: User) {
    let team = user.team;
    delete user.team.members;
    await this.firebaseFirestore
      .collection(TEAMS)
      .doc(team.id)
      .get()
      .subscribe(async resultSnap => {
        let selectedTeam = resultSnap.data() as Team;
        selectedTeam.members.push(user);
        await this.firebaseFirestore
          .collection(TEAMS)
          .doc(team.id)
          .set(selectedTeam);
      });
  }

  saveUserWithImageFile(user: User, profileImage: File) {
    const path = `users/${Date.now()}_${profileImage.name}`;
    const ref = this.storage.ref(path);
    let task = this.storage.upload(path, profileImage);
    task.snapshotChanges().pipe(
      tap(console.log),
      finalize(async () => {
        user.profilePicture = await ref.getDownloadURL().toPromise();
        await this.firebaseFirestore.collection(USERS).doc(user.id).set(user);
      }));
  }

  saveUserWithoutImageFile(user: User) {
    this.firebaseFirestore
      .collection(UTILS)
      .doc('blank_profile')
      .get()
      .subscribe(async value => {
        let profile = value.data() as BlankProfile;
        user.profilePicture = profile.url;
        await this.firebaseFirestore.collection(USERS).doc(user.id).set(user);
      });
  }

}
