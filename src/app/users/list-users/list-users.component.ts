import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {User} from '../../data/user.model';
import {MatPaginator} from '@angular/material/paginator';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {


  users: Array<User>;
  displayedColumns: string[] = ['fullName', 'email', 'team', 'role'];
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private userService: UsersService) {

  }

  ngOnInit(): void {
    this.initTableData();
  }

  initTableData() {
    this.userService
      .listUsers()
      .subscribe(value => {
        this.users = value;
        this.dataSource.data = this.users;
        this.dataSource.paginator = this.paginator;
      });
  }


}
