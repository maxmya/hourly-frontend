import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.authService
        .validateUserAuthentication()
        .subscribe(loggedIn => {
          if (loggedIn) {
            this.router.navigate(['/']);
          }
          subscriber.next(!loggedIn);
          subscriber.complete();
        }, error => {
          subscriber.next(false);
          subscriber.complete();
        });
    });
  }

}
