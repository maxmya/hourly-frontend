import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ResponseWrapper} from '../data/response.wrapper.model';
import {LoginResponse} from '../data/login.response.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {User} from '../data/user.model';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly USERS_KEY = 'users';

  constructor(private firebaseAuth: AngularFireAuth,
              private firestore: AngularFirestore) {
  }


  loginWithEmailAndPassword(email: string, password: string) {
    return new Observable<ResponseWrapper<User>>(subscriber => {
      let response = new ResponseWrapper<User>();
      this.firebaseAuth
        .signInWithEmailAndPassword(email, password)
        .then(value => {
          console.log(value.user.uid);
          response.success = true;
          response.message = 'Welcome';
          this.firestore
            .collection(this.USERS_KEY)
            .doc(value.user.uid)
            .get().subscribe(snapShot => {
            response.data = (snapShot.data() as User);
            subscriber.next(response);
            subscriber.complete();
          });
        })
        .catch(reason => {
          response.data = null;
          response.message = reason.message;
          response.success = false;
          subscriber.next(response);
          subscriber.complete();
        });
    });
  }

  validateUserAuthentication(): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.firebaseAuth
        .authState
        .subscribe(value => {
          if (value == null) {
            subscriber.next(false);
            subscriber.complete();
          } else {
            subscriber.next(true);
            subscriber.complete();
          }
        });
    });
  }

  logoutUser(): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      let done = false;
      this.firebaseAuth
        .signOut()
        .then(_ => {
          done = true;
        })
        .catch(_ => {
          done = false;
        })
        .finally(() => {
          subscriber.next(done);
          subscriber.complete();
        });
    });
  }

}
