import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from './login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  loading = false;
  errorMessage = '';

  constructor(private loginService: LoginService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  submitLogin(loginForm: NgForm) {
    this.errorMessage = '';
    this.loading = true;
    let formVal = loginForm.value;
    this.loginService
      .loginUser(formVal.email, formVal.password)
      .subscribe(response => {
        this.loading = false;
        if (response.success) {
          console.log("NAVIGATE")
          this.router.navigate(['/']);
        } else {
          this.errorMessage = response.message;
        }
      }, error => {
        this.loading = false;
        this.errorMessage = error.error.message;
      });

  }
}
