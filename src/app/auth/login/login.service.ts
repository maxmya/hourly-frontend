import {Injectable} from '@angular/core';
import {AuthService} from '../auth.service';
import {Observable} from 'rxjs';
import {AppCookiesService} from '../../service/app-cookies.service';
import {ResponseWrapper} from '../../data/response.wrapper.model';
import {User} from '../../data/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private authService: AuthService,
              private appCookiesService: AppCookiesService) {
  }

  // return state of login
  loginUser(username: string, password: string): Observable<ResponseWrapper<User>> {
    return new Observable<ResponseWrapper<User>>(subscriber => {
      this.authService
        .loginWithEmailAndPassword(username, password)
        .subscribe(response => {
          if (response.success) {
            this.appCookiesService.saveDisplayName(response.data.fullName);
            this.appCookiesService.savePictureUrl(response.data.profilePicture);
          }
          subscriber.next(response);
          subscriber.complete();
        }, error => {
          subscriber.error(error);
          subscriber.complete();
        });
    });
  }

}
