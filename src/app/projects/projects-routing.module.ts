import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListProjectsComponent} from './list-projects/list-projects.component';
import {MainGuard} from '../main.guard';
import {AddProjectComponent} from './add-project/add-project.component';
import {AdminGuard} from '../admin.guard';

const routes: Routes = [
  {path: '', component: ListProjectsComponent, canActivate: [MainGuard]},
  {path: 'add', component: AddProjectComponent, canActivate: [AdminGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule {
}
