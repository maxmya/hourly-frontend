import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

class DialogData {
  title: string;
  message: string;
  yes: () => any;
  no: () => any;
}

@Component({
  selector: 'yes-no-dialog',
  templateUrl: 'yes-no-dialog.html',
})
export class YesNoDialog {

  constructor(
    private dialogRef: MatDialogRef<YesNoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }


  doYes() {
    this.data.yes();
    this.dialogRef.close();
  }

  doNo() {
    this.data.no();
    this.dialogRef.close();
  }

}
