import {Injectable} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {YesNoDialog} from "./yes-no-dialog";

@Injectable({
  providedIn: 'root'
})
export class YesNoDialogService {

  constructor(private matDialog: MatDialog) {
  }


  openMatDialog(title: string, message: string, yes: () => any, no: () => any) {
    this.matDialog.open(YesNoDialog,
      {
        width: "300",
        data: {title: title, message: message, yes: yes, no: no}
      }
    )
  }

}
