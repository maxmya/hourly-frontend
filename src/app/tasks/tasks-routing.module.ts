import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListTasksComponent} from './list-tasks/list-tasks.component';
import {MainGuard} from '../main.guard';
import {AssignTasksComponent} from './assign-tasks/assign-tasks.component';
import {AdminGuard} from '../admin.guard';

const routes: Routes = [
  {path: '', component: ListTasksComponent, canActivate: [MainGuard]},
  {path: 'assign', component: AssignTasksComponent, canActivate: [AdminGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule {
}
